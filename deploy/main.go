package main

import (
"net/http"
"io/ioutil"
"github.com/buger/jsonparser"
"strconv"
"time"
"bytes"
"encoding/json"
"github.com/go-redis/redis"
"fmt"
"strings"
	"gopkg.in/robfig/cron.v2"
)

type depthResponse map[string]depthResponse2

type depthResponse2 struct{
	Buy []depthResponse3 `json:"buy"`
	Sell []depthResponse3 `json:"sell"`
}

type depthResponse3 struct{
	Quantity string `json:"quantity"`
	Price string `json:"price"`
}

type balanceResponse struct {
	Data []balanceResponseDetail `json:"data"`
}

type balanceResponseDetail struct {
	Type string `json:"type"`
	Id string `json:"id"`
	Attributes balanceResponseDetail2 `json:"attributes"`
}

type balanceResponseDetail2 struct{
	Value float64 `json:"value"`
}

type accountsResponse struct {
	Data accountsResponseDetail `json:"data"`
}

type accountsResponseDetail struct {
	Type string `json:"type"`
	Id string `json:"id"`
}

type ordersResponse struct {
	Data []ordersResponseDetail `json:"data"`
}

type ordersResponseDetail struct {
	Type string `json:"type"`
	Id string `json:"id"`
	Attributes ordersResponseDetail2 `json:"attributes"`
}

type ordersResponseDetail2 struct {
	OrderId int `json:"orderId"`
	AccountId string `json:"accountId"`
	Quantity int `json:"quantity"`
	MatchPrice float64 `json:"matchPrice"`
	Fee float64 `json:"fee"`
	Tax float64 `json:"tax"`
	MetalValue float64 `json:"metalValue"`
	SettlementAmount float64 `json:"settlementAmount"`
	CreatedAt string `json:"createdAt"`
	DeliveryDate string `json:"deliveryDate"`
}

type message struct {
	Content string
}

func main() {
	rds := conn()
	defer rds.Close()

	scheduleCache()

	mux := http.NewServeMux()
	mux.HandleFunc("/apiv1/abx/contract", orderBuy)
	mux.HandleFunc("/apiv1/abx/order", fundAndOrder(rds))
	mux.HandleFunc("/apiv1/abx/createAccount", createAccount(rds))
	mux.HandleFunc("/apiv1/abx/balance", balance(rds))
	mux.HandleFunc("/apiv1/abx/depth", depth())
	mux.HandleFunc("/health", health)
	http.ListenAndServe(":6040", mux)
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     "35.197.155.228:9967",
		Password: "t3guhCakep", // no password set
		DB:       2,  // use default DB
	})
	return client
}

func health(w http.ResponseWriter, r *http.Request){
	/*p := message{Content:"STATUS : HEALTH"}
	t,_ := template.ParseFiles("deploy/index.html");
	t.Execute(w,p);*/

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

var accountId = "d28efb51-d760-11e5-8257-026102b72b97"
var contractId = "658"
var marketplaceId = 1
var balanceTypeId = "Holdings"
var authorization = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfbWV0YWRhdGEiOnsibWV0YWxkZXNrX3N5c3RlbSI6IkdMT0JBTCIsIm1ldGFsZGVza19tZmEiOmZhbHNlLCJtZXRhbGRlc2tfYWNjb3VudF9pZCI6NDMzMSwibWV0YWxkZXNrX3VzZXJfaWQiOjMzMzZ9LCJpYXQiOjE1MDUzNDYzNTIsImV4cCI6MTU0ODU0NjM1Mn0.IDy34ezOQFUuF5XYBEw0J3g8JQUy-3nAx9HbuGqveZE"

var urlCreateAccount = "https://uat-live-demo.abx.com/api/v1.0/account/private_client/individual"

func orderBuy(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	status := http.StatusOK
	quantity,_ := jsonparser.GetInt(body,"quantity")
	t := time.Now()
	timestamp := t.Format("2006-01-02T15:04:05.999Z")

	client := &http.Client{}
	bodyParam := `{"data":{"attributes":{"accountId":"`+accountId+`","contractId":"`+contractId+`","direction":"buy","quantity":`+strconv.FormatInt(quantity,10)+`,"orderType":"market","limitPrice":0,"validity":"GTC","expiryDate":"2017-11-06T04:52:20.961Z","clientTimestamp":"`+timestamp+`"}}}`

	req, err := http.NewRequest("POST", "https://uat-live-demo.abx.com/api/v1.0/orders", bytes.NewBuffer([]byte(bodyParam)))
	if err != nil{
		println("FAILED : "+err.Error())
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("authorization", authorization)
	resp, err := client.Do(req)
	response := ordersResponse{}
	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	if err == nil {
		statusCode := resp.StatusCode
		err = json.Unmarshal(bodyBytes, &response)
		if err == nil {
			if statusCode == http.StatusCreated || statusCode == http.StatusOK{

			}
		}else{
			println("FAILED : "+err.Error())
		}
	}else{
		println("FAILED : "+err.Error())
	}


	w.WriteHeader(status)
	w.Write([]byte(""))
}

func fundAndOrder(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		status := http.StatusOK
		message := ""
		amount, _ := jsonparser.GetInt(body, "amount") //gram
		cashtag, _ := jsonparser.GetString(body, "cashtag")
		direction, _ := jsonparser.GetString(body, "direction")
		t := time.Now()
		timestamp := t.Format("2006-01-02T15:04:05.999Z")

		quantity := float64(amount) / 0.01
		val, err := redist.Get(cashtag).Result()
		if err == redis.Nil || val == "" {
			status = http.StatusNotFound
			message = `{"message":"Akun tidak ditemukan"}`
		} else {
			client := &http.Client{}
			bodyParam := fmt.Sprintf(`{"data":{"attributes":{"accountId":"%s","contractId":"%s","direction":"%s","quantity":%f,"orderType":"market","clientTimestamp":"%s"}}}`,val,contractId,direction,quantity,timestamp)

			req, err := http.NewRequest("POST", "https://uat-live-demo.abx.com/api/v1.0/fx/USD-IDR/fund_and_order", bytes.NewBuffer([]byte(bodyParam)))
			if err 	!= nil {
				status = http.StatusInternalServerError
				message = `{"message":"` + err.Error() + `"}`
			}
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("authorization", authorization)
			resp, err := client.Do(req)
			response := ordersResponse{}
			bodyBytes,_ := ioutil.ReadAll(resp.Body)
			if err == nil {
				statusCode := resp.StatusCode
				err = json.Unmarshal(bodyBytes, &response)
				if err == nil {
					if statusCode == http.StatusCreated || statusCode == http.StatusOK {
						qty := strconv.Itoa(response.Data[0].Attributes.Quantity)
						matchPrice := response.Data[0].Attributes.MatchPrice
						fee := response.Data[0].Attributes.Fee
						tax := response.Data[0].Attributes.Tax
						metalValue := response.Data[0].Attributes.MetalValue
						settlementAmount := response.Data[0].Attributes.SettlementAmount
						message = fmt.Sprintf(`{"orderId":%d,"accountId":"%s","quantity":%s,"matchPrice":%f,"fee":%f,"tax":%f,"metalValue":%f,"settlementAmount":%f}`,response.Data[0].Attributes.OrderId,response.Data[0].Attributes.AccountId,qty,matchPrice,fee,tax,metalValue,settlementAmount)
					}
				} else {
					status = http.StatusBadRequest
					message = `{"message":"` + err.Error() + `"}`
				}
			} else {
				status = http.StatusBadRequest
				message = `{"message":"` + err.Error() + `"}`
			}
		}

		w.WriteHeader(status)
		w.Write([]byte(message))
	}
}

func createAccount(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request){
		body, _ := ioutil.ReadAll(r.Body)
		status := http.StatusOK
		message := ""
		cashtag,_ := jsonparser.GetString(body,"cashtag")

		_, err := redist.Get(cashtag).Result()
		if err == redis.Nil {
			client := &http.Client{}
			bodyParam := `{"data": {"type": "Accounts","attributes": {"sponsorAccountId":"`+accountId+`","firstName":"`+cashtag+`","lastName":"","email":""}}}`

			req, err := http.NewRequest("POST", urlCreateAccount, bytes.NewBuffer([]byte(bodyParam)))
			if err != nil{
				message = err.Error()
			}
			req.Header.Set("Content-Type", "application/json")
			req.Header.Set("authorization", authorization)
			resp, err := client.Do(req)
			response := accountsResponse{}
			bodyBytes, _ := ioutil.ReadAll(resp.Body)
			if err == nil {
				statusCode := resp.StatusCode
				err = json.Unmarshal(bodyBytes, &response)
				if err == nil {
					if statusCode == http.StatusCreated || statusCode == http.StatusOK{
						redist.Set(cashtag,response.Data.Id,0)
						message = "Akun Berhasil Dibuat"
					}
				}else{
					status = http.StatusInternalServerError
					message = err.Error()
				}
			}else{
				status = http.StatusInternalServerError
				message = err.Error()
			}
		}else{
			status = http.StatusConflict
			message = "Akun Sudah Terdaftar"
		}
		w.WriteHeader(status)
		w.Write([]byte(`{"message":"`+message+`"}`))
	}
}

func balance(redist *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		status := http.StatusOK
		message := ""
		cashtag, _ := jsonparser.GetString(body, "cashtag")

		val, err := redist.Get(cashtag).Result()
		if err == redis.Nil || val == "" {
			status = http.StatusNotFound
			message = `{"message":"Akun tidak ditemukan"}`
		} else {
			client := &http.Client{}
			strMarketplaceId := strconv.Itoa(marketplaceId)
			//bodyParam := fmt.Sprintf(`{"data":{"attributes":{"marketplaceId":"%d","contractId":"%d","balanceTypeId":"%s","accountId":"%s"}}}`,marketplaceId,intContractId,balanceTypeId,val)

			req, err := http.NewRequest("GET", "https://uat-live-demo.abx.com/api/v1.0/balances/holdings", bytes.NewBuffer([]byte("")))
			if err 	!= nil {
				status = http.StatusInternalServerError
				message = `{"message":"` + err.Error() + `"}`
			}else {
				req.Header.Set("Content-Type", "application/json")
				req.Header.Set("authorization", authorization)

				q := req.URL.Query()
				q.Add("marketplaceId", strMarketplaceId)
				q.Add("contractId", contractId)
				q.Add("balanceTypeId", balanceTypeId)
				q.Add("accountId", val)
				req.URL.RawQuery = q.Encode()

				resp, err := client.Do(req)
				response := balanceResponse{}
				bodyBytes, _ := ioutil.ReadAll(resp.Body)
				if err == nil {
					statusCode := resp.StatusCode
					err = json.Unmarshal(bodyBytes, &response)
					if err == nil {
						if statusCode == http.StatusCreated || statusCode == http.StatusOK {
							value := response.Data[0].Attributes.Value
							message = fmt.Sprintf(`{"accountId":"%s","balanceTypeId":"%s","marketplaceId":%d,"contractId":%s,"value":%f}`, accountId, balanceTypeId, marketplaceId, contractId, value)
						}
					} else {
						status = http.StatusBadRequest
						message = `{"message":"` + err.Error() + `"}`
					}
				} else {
					status = http.StatusBadRequest
					message = `{"message":"` + err.Error() + `"}`
				}
			}
		}

		w.WriteHeader(status)
		w.Write([]byte(message))
	}
}

func depth() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		url := "https://uat-live-demo.abx.com/api/v1.0/fx/USD-IDR/depths?marketplaceId=1"

		req, _ := http.NewRequest("GET", url, nil)

		req.Header.Add("authorization", authorization)

		res, err := http.DefaultClient.Do(req)

		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		var f interface{}
		err = json.Unmarshal(body, &f)
		if err != nil {
			fmt.Println("Error parsing JSON: ", err)
		}

		status := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`
		var buy float64 = 0
		var sell float64 = 0
		// JSON object parses into a map with string keys
		itemsMap := f.(map[string]interface{})
		for k, v := range itemsMap {
			if(strings.Compare(k,contractId) == 0){
				switch jsonObj := v.(type) {
				case interface{}:
					//var item depthResponse2
					// Access the values in the JSON object and place them in an Item
					for itemKey, itemValue := range jsonObj.(map[string]interface{}) {
						if(strings.Compare(strings.ToLower(itemKey),"buy") == 0){
							buy = getHighestValue(itemValue)
						}
						if(strings.Compare(strings.ToLower(itemKey),"sell") == 0){
							sell = getHighestValue(itemValue)
						}
					}
					status = http.StatusOK
					message = fmt.Sprintf(`{"buy":%.2f,"sell":%.2f}`,buy,sell)
				default:
					fmt.Println("Unknown key for Item found in JSON")
				}
			}
		}
		w.WriteHeader(status)
		w.Write([]byte(message))

	}
}

func getHighestValue(itemValue interface{}) (float64) {
	var comparator float64 = 0
	switch jsonObjDetail := itemValue.(type) {
	case interface{}:
		for _, values := range jsonObjDetail.([]interface{}) {
			switch jsonObjDetails := values.(type) {
			case interface{}:
				for key, value := range jsonObjDetails.(map[string]interface{}) {
					if(strings.Compare(strings.ToLower(key),"price") == 0) {
						switch i := value.(type) {
						case float64:
							if (i > comparator) {
								comparator = i
							}
						default:
							i = 0
						}
					}
				}
			default:
				fmt.Println("Unknown key for Item found in JSON")
			}
		}
	default:
		fmt.Println("Unknown key for Item found in JSON")
	}
	return comparator
}

func scheduleCache(){
	fmt.Println("start")
	c := cron.New()
	c.AddFunc("@every 1m", func() {
		fmt.Println("1 min")
		url := "https://uat-live-demo.abx.com/api/v1.0/fx/USD-IDR/depths?marketplaceId=1"
		req, _ := http.NewRequest("GET", url, nil)
		req.Header.Add("authorization", authorization)
		res, _ := http.DefaultClient.Do(req)
		defer res.Body.Close()
		})
	c.Start()
}